﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BFSAPO.NordFK;
using Microsoft.Extensions.Configuration;
 
using System.ServiceModel;
 
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
 
using Microsoft.AspNetCore.Mvc;

namespace Glofitech.BFSAbsorber.Nord
{
    public class Util
    {

        public static BFSAPO.NordFK.bfsapiSoapClient GetBfsapiSoapClient(IConfiguration configuration)
        {

            BFSAPO.NordFK.bfsapiSoapClient.EndpointConfiguration endpoint = new BFSAPO.NordFK.bfsapiSoapClient.EndpointConfiguration();
 
            var client = new BFSAPO.NordFK.bfsapiSoapClient(endpoint);
            client.ChannelFactory.Endpoint.Address = new System.ServiceModel.EndpointAddress(configuration["APIClientDetails:ServiceUrl"]);

            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = Int32.MaxValue;
            binding.ReceiveTimeout = TimeSpan.MaxValue;
            binding.SendTimeout = TimeSpan.MaxValue;
            binding.ReaderQuotas.MaxArrayLength = Int32.MaxValue;
            binding.ReaderQuotas.MaxStringContentLength = Int32.MaxValue;
            binding.ReaderQuotas.MaxBytesPerRead = Int32.MaxValue;

             

            binding.Security.Mode = BasicHttpSecurityMode.Transport;

            client.ChannelFactory.Endpoint.Binding = binding;



            return client;
        }

        


        public static object  GetArgsFromQueryCollection(object Args , IQueryCollection collection   )
        {
            object value = new object();

            foreach (string key in collection.Keys)
            {
                value = collection[key].ToString();


                if ( Args.GetType().GetProperty(key).PropertyType == typeof(DateTime) |  Args.GetType().GetProperty(key).PropertyType == typeof(Nullable<DateTime>))

                {
                    value = Convert.ToDateTime(collection[key].ToString());
                }

                if (Args.GetType().GetProperty(key).PropertyType == typeof(Boolean) | Args.GetType().GetProperty(key).PropertyType == typeof(Nullable<Boolean>))

                {
                    value = Convert.ToBoolean(collection[key].ToString());
                }


                if ( Args.GetType().GetProperty(key).PropertyType == typeof(Int32) |  Args.GetType().GetProperty(key).PropertyType == typeof(Nullable<Int32>))
                {
                    value = Convert.ToInt32(collection[key].ToString());
                }



                if ( Args.GetType().GetProperty(key).PropertyType == typeof(decimal) | Args.GetType().GetProperty(key).PropertyType == typeof(Nullable<decimal>))
                {
                    value = Convert.ToDecimal( collection[key].ToString());
                }


                if ( Args.GetType().GetProperty(key).PropertyType == typeof(double) |  Args.GetType().GetProperty(key).PropertyType == typeof(Nullable<double>))
                {
                    value = Convert.ToDecimal(collection[key].ToString());
                }

                if ( Args.GetType().GetProperty(key).PropertyType == typeof(Guid) |  Args.GetType().GetProperty(key).PropertyType == typeof(Nullable<Guid>))
                {
                    value = Guid.Parse(collection[key].ToString());
                }

                if (Args.GetType().GetProperty(key).PropertyType == typeof(string[])  )

                {
 
                    value = collection[key].ToString().Split(',', StringSplitOptions.RemoveEmptyEntries);
 
                }

                if (Args.GetType().GetProperty(key).PropertyType == typeof(Guid[]))

                {

                   var tempArray    = collection[key].ToString().Split(',', StringSplitOptions.RemoveEmptyEntries);

                    value = tempArray.Select(x=> Guid.Parse(x)).ToArray();

                }

                if (Args.GetType().GetProperty(key).PropertyType == typeof(int[]))

                {

                    var tempArray = collection[key].ToString().Split(',', StringSplitOptions.RemoveEmptyEntries);

                    value = tempArray.Select(x => int.Parse(x)).ToArray();

                }

                Args.GetType().GetProperty(key).SetValue(Args, value);

            }

            return Args;
        }


        public static bool IsIpAddressAllowed(string IpAddress, IConfiguration config)
        {
            if (config["APIClientDetails:AllowedIPAddresses"] == "*")
                return true;

            if (!string.IsNullOrWhiteSpace(IpAddress))
            {
                string[] addresses = Convert.ToString(config["APIClientDetails:AllowedIPAddresses"]).Split(',');
                return addresses.Where(a => a.Trim().Equals(IpAddress, StringComparison.InvariantCultureIgnoreCase)).Any();
            }
            return false;
        }
    }
}

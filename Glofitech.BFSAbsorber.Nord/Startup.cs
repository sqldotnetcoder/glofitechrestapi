﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Glofitech.BFSAbsorber.Nord.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Http;
using ZNetCS.AspNetCore.Authentication.Basic;
using ZNetCS.AspNetCore.Authentication.Basic.Events;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;

namespace Glofitech.BFSAbsorber.Nord
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<APIClientDetails>(Configuration.GetSection("APIClientDetails"));

            services
            .AddAuthentication(BasicAuthenticationDefaults.AuthenticationScheme)
            .AddBasicAuthentication(
           options =>
           {
               options.Realm = "My Application";
               options.Events = new BasicAuthenticationEvents
               {
                   OnValidatePrincipal = context =>
                   {
                       if ((context.UserName ==Configuration["APIClientDetails:RestAPIUserName"]) && (context.Password == Configuration["APIClientDetails:RestAPIKey"]))
                       {
                           var claims = new List<Claim>
                           {
                                new Claim(ClaimTypes.Name, context.UserName, context.Options.ClaimsIssuer)
                           };

                           var principal = new ClaimsPrincipal(new ClaimsIdentity(claims, BasicAuthenticationDefaults.AuthenticationScheme));
                           context.Principal = principal;
                       }
                       else
                       {
                            // optional with following default.
                            // context.AuthenticationFailMessage = "Authentication failed."; 
                        }

                       return Task.CompletedTask;
                   }
               };
           });


            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            //services.Configure<MvcOptions>(options =>
            //{
            //    options.Filters.Add(new RequireHttpsAttribute());
            //});

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

       


            app.UseAuthentication();

            app.UseMvc();
        }

    }
}

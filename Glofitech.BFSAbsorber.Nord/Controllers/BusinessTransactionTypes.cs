﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using BFSAPO.NordFK;
namespace Glofitech.BFSAbsorber.Nord.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class BusinessTransactionTypesController : Controller
    {
        IConfiguration config;
        private IHttpContextAccessor _accessor;

        public BusinessTransactionTypesController(IConfiguration iconfiguration, IHttpContextAccessor accessor)
        {
            config = iconfiguration;
            _accessor = accessor;
        }
        // GET api/values
        [HttpGet]
        public async Task<BFSAPO.NordFK.GetBusinessTransactionTypeResponseRow[]> Get()
        {

            string ipAddress = HttpContext.Connection.RemoteIpAddress.ToString().Trim();

            if (!Util.IsIpAddressAllowed(ipAddress, config))
            {
                List<string> a = new List<string>();
                a.Add(ipAddress);
                return null;
            }



            var allFields = new BFSAPO.NordFK.GetBusinessTransactionTypeFields();
            foreach (var p in allFields.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {

                p.SetValue(allFields, true);
            }

            GetBusinessTransactionTypeRequest request = new GetBusinessTransactionTypeRequest();
            request.Fields = allFields;
            request.Credentials = new Credentials();
            request.Credentials.UserName = config["APIClientDetails:UserName"];
            request.Credentials.Password = config["APIClientDetails:Password"];
            request.identify = config["APIClientDetails:APIKey"];

            request.Args = new GetBusinessTransactionTypeArgs();

 

            object value = new object();

            request.Args = (GetBusinessTransactionTypeArgs)Util.GetArgsFromQueryCollection(request.Args, HttpContext.Request.Query);



            var client = Util.GetBfsapiSoapClient(config);
            var retval = await client.GetBusinessTransactionTypesAsync(request);

            return retval.Result;

        }


    }
}
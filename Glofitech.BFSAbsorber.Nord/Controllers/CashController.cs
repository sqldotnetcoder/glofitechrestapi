﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using BFSAPO.NordFK;
namespace Glofitech.BFSAbsorber.Nord.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class BusinessTransactionsController : Controller
    {
        IConfiguration config;
        private IHttpContextAccessor _accessor;

        public BusinessTransactionsController(IConfiguration iconfiguration, IHttpContextAccessor accessor)
        {
            config = iconfiguration;
            _accessor = accessor;
        }
        // GET api/values
        [HttpGet]
        public async Task<BFSAPO.NordFK.GetBusinessTransactionResponseRow[]> Get()
        {

            string ipAddress = HttpContext.Connection.RemoteIpAddress.ToString().Trim();

            if (!Util.IsIpAddressAllowed(ipAddress, config))
            {
                List<string> a = new List<string>();
                a.Add(ipAddress);
                return null;
            }



            var allFields = new BFSAPO.NordFK.GetBusinessTransactionFields();
            foreach (var p in allFields.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {

                p.SetValue(allFields, true);
            }

            GetBusinessTransactionsRequest request = new GetBusinessTransactionsRequest();
            request.Fields = allFields;
            request.Credentials = new Credentials();
            request.Credentials.UserName = config["APIClientDetails:UserName"];
            request.Credentials.Password = config["APIClientDetails:Password"];
            request.identify = config["APIClientDetails:APIKey"];

            request.Args = new GetBusinessTransactionArgs();

 

       
            request.Args = (GetBusinessTransactionArgs)Util.GetArgsFromQueryCollection(request.Args, HttpContext.Request.Query);



            var client = Util.GetBfsapiSoapClient(config);
            var retval = await client.GetBusinessTransactionsAsync(request);

            return retval.Result;

        }


    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;

namespace Glofitech.BFSAbsorber.Nord.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class TestController : Controller
    {
        IConfiguration config;
        private IHttpContextAccessor _accessor;

        public TestController(IConfiguration iconfiguration, IHttpContextAccessor accessor)
        {
            config = iconfiguration;
            _accessor = accessor;
        }

        private bool IsIpAddressAllowed(string IpAddress)
        {
            if (!string.IsNullOrWhiteSpace(IpAddress))
            {
                string[] addresses = Convert.ToString(config["APIClientDetails:AllowedIPAddresses"]).Split(',');
                return addresses.Where(a => a.Trim().Equals(IpAddress, StringComparison.InvariantCultureIgnoreCase)).Any();
            }
            return false;
        }

        // GET api/values
        [HttpGet]
        public BFSAPO.NordFK.GetPersonResponse Get()
        {
            string ipAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();

            if (!IsIpAddressAllowed(ipAddress.Trim()))
            {
                List<string> a = new List<string>();
                a.Add(ipAddress);
                return null;
            }

            string uName = "";
            string uPass = "";
            string bfsApiKey = "";

            uName = config["APIClientDetails:UserName"];
            uPass = config["APIClientDetails:Password"];
            bfsApiKey = config["APIClientDetails:APIKey"];


            var client = Util.GetBfsapiSoapClient(config);

            var credentials = new BFSAPO.NordFK.Credentials()
            {
                UserName = uName,
                Password = uPass,
            };
            var persons = client.GetPersonsAsync(new BFSAPO.NordFK.GetPersonRequest()
            {
                Credentials = credentials,
                identify = bfsApiKey, //Identifier is a unique token for your instance of BFS
                Fields = new BFSAPO.NordFK.GetPersonFields()
                {
                    BrickId = true,
                    CreatedDate = true,
                    FirstName = true,
                    LastName = true,
                    MiddleNames = true,
                    UserName = true,
                    Email = true,
                    PersonalNumber = true,
                    BirthDate = true,
                    ResellerNo = true,
                    ResellerId = true,
                    IsApproved = true,
                    AddressCity = true,
                    AddressStreet = true,
                    AddressZip = true,
                    Comment = true,
                    ExternalReference = true,
                    PassportNumber = true,
                    PhoneHome = true,
                    PhoneWork = true,
                    IsTaxPayer = true,
                    IsInsuranceCompany = true,
                    IsInsuranceProductSupplier = true,
                    IsApprovedForStructs = true,
                    IsVerified = true,
                    IsFundEntity = true,
                    IsFundCompany = true,
                    IsIssuer = true,
                    SectorNACE = true,
                    GroupCode = true,
                    ExternalRating = true,
                    RatingAgency = true,
                    InstrumentTypesString = true,

              
                    Country = true,
                    TaxCountry = true,
                    CustomerNo = true,
                    BIC = true,
                    LastLoginDate = true,
                    UserDomain = true,




                    PostageAddressStreet = true,
                    PostageAddressCO = true,
                    PostageAddressZip = true,
                    PostageAddressCity = true,
                    PostageAddressCountry = true,
                    PostageAddressDepartment = true,
                    CustomFields = true,
                    LEI = true,
                    TRSId = true,
                    TRSCountry = true,
                    TRSIdType = true,
                    TRSManualHandling = true,
                    IsProfessional = true,
                    MifidOk = true,
                    IsPEP = true,
                    BranchCountry = true,
                    DefaultCompany = true,
                    AccessLevel = true,
                },
            });

            return persons.Result;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using System.Reflection;
using BFSAPO.NordFK;

namespace Glofitech.BFSAbsorber.Nord.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class HistoricPricesController : Controller
    {
        IConfiguration config;
        private IHttpContextAccessor _accessor;

        public HistoricPricesController(IConfiguration iconfiguration, IHttpContextAccessor accessor)
        {
            config = iconfiguration;
            _accessor = accessor;
        }



        // GET api/values
        [HttpGet]
        public BFSAPO.NordFK.GetHistoricPricesResponse Get()
        {
            string ipAddress = HttpContext.Connection.RemoteIpAddress.ToString().Trim();

            if (!Util.IsIpAddressAllowed(ipAddress, config))
            {
                List<string> a = new List<string>();
                a.Add(ipAddress);
                return null;
            }



            string uName = "";
            string uPass = "";
            string bfsApiKey = "";

            uName = config["APIClientDetails:UserName"];
            uPass = config["APIClientDetails:Password"];
            bfsApiKey = config["APIClientDetails:APIKey"];

            var client = Util.GetBfsapiSoapClient(config);

            var credentials = new BFSAPO.NordFK.Credentials()
            {
                UserName = uName,
                Password = uPass,
            };

            var instruments = getHistoricPrices().Result;

            List<Guid> idList = new List<Guid>();
            foreach (var unit in instruments)
            {
                idList.Add(unit.BrickId);
            }

            Guid[] brickIdList = new Guid[idList.Count];
            int unitCount = 0;
            foreach (Guid unit in idList)
            {
                brickIdList[unitCount] = unit;
            }



            var allFields = new GetHistoricPricesFields();
            foreach (var p in allFields.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {

                p.SetValue(allFields, true);
            }

            GetHistoricPricesRequest request = new GetHistoricPricesRequest();
            request.Fields = allFields;
            request.Credentials = new Credentials();
            request.Credentials.UserName = config["APIClientDetails:UserName"];
            request.Credentials.Password = config["APIClientDetails:Password"];
            request.identify = config["APIClientDetails:APIKey"];

            request.Args = new GetHistoricPricesArgs();


            request.Args = (GetHistoricPricesArgs)Util.GetArgsFromQueryCollection(request.Args, HttpContext.Request.Query);

            request.Args.AssetIds = brickIdList;

            if (request.Args.FromDate == null)
                request.Args.FromDate = DateTime.Now.AddDays(-14);

            if (request.Args.ToDate == null)
                request.Args.ToDate = DateTime.Now;


            var historicPrices = client.GetHistoricPricesAsync( request      );

            return historicPrices.Result;
        }

        public BFSAPO.NordFK.GetInstrumentsResponse getHistoricPrices()
        {


            string uName = "";
            string uPass = "";
            string bfsApiKey = "";

            uName = config["APIClientDetails:UserName"];
            uPass = config["APIClientDetails:Password"];
            bfsApiKey = config["APIClientDetails:APIKey"];

            var client = new BFSAPO.NordFK.bfsapiSoapClient(new BFSAPO.NordFK.bfsapiSoapClient.EndpointConfiguration());
            var credentials = new BFSAPO.NordFK.Credentials()
            {
                UserName = uName,
                Password = uPass,
            };


            var allFields = new BFSAPO.NordFK.GetInstrumentsFields();

            foreach (var p in allFields.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {

                p.SetValue(allFields, true);
            }

            var instruments = client.GetInstrumentsAsync(new BFSAPO.NordFK.GetInstrumentsRequest()
            {
                Credentials = credentials,
                identify = bfsApiKey, //Identifier is a unique token for your instance of BFS                    
                Fields = new BFSAPO.NordFK.GetInstrumentsFields()
                {
                    BrickId = true,
                    CreatedDate = true
                },
            });

            return instruments.Result;
        }

    }
}

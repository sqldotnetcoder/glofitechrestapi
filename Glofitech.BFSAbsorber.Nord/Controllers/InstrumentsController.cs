﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using System.Reflection;
using BFSAPO.NordFK;

namespace Glofitech.BFSAbsorber.Nord.Controllers
{

     [Authorize]
    [Route("api/[controller]")]
    public class InstrumentsController : Controller
    {
        IConfiguration config;
        private IHttpContextAccessor _accessor;

        public InstrumentsController(IConfiguration iconfiguration, IHttpContextAccessor accessor)
        {
            config = iconfiguration;
            _accessor = accessor;
        }

 

        // GET api/values

        [HttpGet]
        public async Task<GetInstrumentsResponseRow[]> Get()
        {


            string ipAddress = HttpContext.Connection.RemoteIpAddress.ToString().Trim();

            if (!Util.IsIpAddressAllowed(ipAddress, config))
            {
                List<string> a = new List<string>();
                a.Add(ipAddress);
                return null;
            }
 

            string uName = "";
            string uPass = "";
            string bfsApiKey = "";

            uName = config["APIClientDetails:UserName"];
            uPass = config["APIClientDetails:Password"];
            bfsApiKey = config["APIClientDetails:APIKey"];

 

            var credentials = new Credentials()
            {
                UserName = uName,
                Password = uPass,
            };

            var allFields = new GetInstrumentsFields();



            foreach (var p in allFields.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {

                p.SetValue(allFields, true);
            }
            allFields.InstrumentCategorization = true;

            GetInstrumentsRequest request = new GetInstrumentsRequest();
          
            request.Credentials = credentials;
            request.identify = bfsApiKey;
            request.Fields = allFields;

            request.Args = new GetInstrumentsArgs();



            object value = new object();

            request.Args = (GetInstrumentsArgs)Util.GetArgsFromQueryCollection(request.Args, HttpContext.Request.Query);

            var client = Util.GetBfsapiSoapClient(config);
            var instruments = await client.GetInstrumentsAsync(request);

            var retval = instruments.Result;


            return retval;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

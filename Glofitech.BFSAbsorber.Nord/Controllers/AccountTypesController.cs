﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using BFSAPO.NordFK;
namespace Glofitech.BFSAbsorber.Nord.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class AccountTypesController : Controller
    {
        IConfiguration config;
        private IHttpContextAccessor _accessor;

        public AccountTypesController(IConfiguration iconfiguration, IHttpContextAccessor accessor)
        {
            config = iconfiguration;
            _accessor = accessor;
        }
        // GET api/values
        [HttpGet]
        public async Task<BFSAPO.NordFK.GetAccountTypeResponseRow[]> Get()
        {

            string ipAddress = HttpContext.Connection.RemoteIpAddress.ToString().Trim();

            if (!Util.IsIpAddressAllowed(ipAddress, config))
            {
                List<string> a = new List<string>();
                a.Add(ipAddress);
                return null;
            }



            var allFields = new BFSAPO.NordFK.GetAccountTypeFields();
            foreach (var p in allFields.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {

                p.SetValue(allFields, true);
            }

            GetAccountTypeRequest getAccountTypesRequest = new GetAccountTypeRequest();
            getAccountTypesRequest.Fields = allFields;
            getAccountTypesRequest.Credentials = new Credentials();
            getAccountTypesRequest.Credentials.UserName = config["APIClientDetails:UserName"];
            getAccountTypesRequest.Credentials.Password = config["APIClientDetails:Password"];
            getAccountTypesRequest.identify = config["APIClientDetails:APIKey"];

            getAccountTypesRequest.Args = new GetAccountTypeArgs();

            Console.WriteLine(HttpContext.Request.Query);

            object value = new object();

            getAccountTypesRequest.Args =(GetAccountTypeArgs) Util.GetArgsFromQueryCollection(getAccountTypesRequest.Args, HttpContext.Request.Query);

             

            var client = Util.GetBfsapiSoapClient(config);
            var retval = await client.GetAccountTypesAsync(getAccountTypesRequest);

            return retval.Result;

        }

        
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;

namespace Glofitech.BFSAbsorber.Nord.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class HistoricPositionsController : Controller
    {
        IConfiguration config;
        private IHttpContextAccessor _accessor;

        public HistoricPositionsController(IConfiguration iconfiguration, IHttpContextAccessor accessor)
        {
            config = iconfiguration;
            _accessor = accessor;
        }

        private bool IsIpAddressAllowed(string IpAddress)
        {
            if (!string.IsNullOrWhiteSpace(IpAddress))
            {
                string[] addresses = Convert.ToString(config["APIClientDetails:AllowedIPAddresses"]).Split(',');
                return addresses.Where(a => a.Trim().Equals(IpAddress, StringComparison.InvariantCultureIgnoreCase)).Any();
            }
            return false;
        }

        // GET api/values
        [HttpGet]
        public BFSAPO.NordFK.GetHistoricPositionResponse Get()
        {     
            string ipAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            var headers = _accessor.HttpContext.Request.Headers;
            string token = headers["Authorization"];
            

            if (!IsIpAddressAllowed(ipAddress.Trim()))
            {
                List<string> a = new List<string>();
                a.Add(ipAddress);
                return null;
            }

            string uName = "";
            string uPass = "";
            string bfsApiKey = "";

            uName = config["APIClientDetails:UserName"];
            uPass = config["APIClientDetails:Password"];
            bfsApiKey = config["APIClientDetails:APIKey"];

            var client = Util.GetBfsapiSoapClient(config);

            var credentials = new BFSAPO.NordFK.Credentials()
            {
                UserName = uName,
                Password = uPass,
            };

            var historicPositions = client.GetHistoricPositionsAsync(new BFSAPO.NordFK.GetHistoricPositionRequest()
            {
                Credentials = credentials,
                identify = bfsApiKey, //Identifier is a unique token for your instance of BFS
                Args = new BFSAPO.NordFK.GetHistoricPositionArgs()
                {
                    StartDate = DateTime.Today.AddDays(-5),
                    EndDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day, DateTime.Today.Hour, DateTime.Today.Minute, DateTime.Today.Second, DateTimeKind.Utc),
                    DisplayCurrencyCode = "SEK",
                    AccountDimensionKey = "T"
                },
                Fields = new BFSAPO.NordFK.GetHistoricPositionFields()
                {
                    Account = true,
                    AccountNo = true,
                    AccountDimensionKey = true,
                    Amount = true,
                    Asset = true,
                    AssetType = true,
                    BalanceDate = true,
                    BaseRate = true,
                    DisplayCurrencyCode = true,
                    DisplayRate = true,
                    FxRate = true,
                    MarketValue = true,
                    MarketValueAccountCurrency = true,
                    MarketValueDisplayCurrency = true,
                    Price = true,
                    DisplayMultiplier = true,
                    DisplayPercentagePrice = true ,
                    ValueMultiplier=true

                },
            });

            return historicPositions.Result;
        }

        public BFSAPO.NordFK.GetInstrumentsResponse getHistoricPrices()
        {
            string ipAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();

            if (!IsIpAddressAllowed(ipAddress.Trim()))
            {
                List<string> a = new List<string>();
                a.Add(ipAddress);
                return null;
            }

            string uName = "";
            string uPass = "";
            string bfsApiKey = "";

            uName = config["APIClientDetails:UserName"];
            uPass = config["APIClientDetails:Password"];
            bfsApiKey = config["APIClientDetails:APIKey"];

            var client = new BFSAPO.NordFK.bfsapiSoapClient(new BFSAPO.NordFK.bfsapiSoapClient.EndpointConfiguration());
            var credentials = new BFSAPO.NordFK.Credentials()
            {
                UserName = uName,
                Password = uPass,
            };
            var instruments = client.GetInstrumentsAsync(new BFSAPO.NordFK.GetInstrumentsRequest()
            {
                Credentials = credentials,
                identify = bfsApiKey, //Identifier is a unique token for your instance of BFS                    
                Fields = new BFSAPO.NordFK.GetInstrumentsFields()
                {
                    BrickId = true,
                    CreatedDate = true
                },
            });

            return instruments.Result;
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}

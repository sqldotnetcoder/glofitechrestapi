﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using BFSAPO.NordFK;


namespace Glofitech.BFSAbsorber.Nord.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class AccountsController : Controller
    {
        IConfiguration config;
        private IHttpContextAccessor _accessor;

        public AccountsController(IConfiguration iconfiguration, IHttpContextAccessor accessor)
        {
            config = iconfiguration;
            _accessor = accessor;
        }
        // GET api/values
        [HttpGet]
        public async Task<IActionResult> Get()
        {
         

            string ipAddress = HttpContext.Connection.RemoteIpAddress.ToString().Trim();

            if (!Util.IsIpAddressAllowed(ipAddress, config))
            {
            
                return  null;
            }



            var allFields = new BFSAPO.NordFK.GetAccountFields();
            foreach (var p in allFields.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {

                p.SetValue(allFields, true);
            }

            GetAccountsRequest getAccountsRequest = new GetAccountsRequest();
            getAccountsRequest.Fields = allFields;
            getAccountsRequest.Credentials = new Credentials();
            getAccountsRequest.Credentials.UserName = config["APIClientDetails:UserName"];
            getAccountsRequest.Credentials.Password = config["APIClientDetails:Password"];
            getAccountsRequest.identify = config["APIClientDetails:APIKey"];

            getAccountsRequest.Args = new GetAccountsArgs();
 
            getAccountsRequest.Args =(GetAccountsArgs) Util.GetArgsFromQueryCollection(getAccountsRequest.Args, HttpContext.Request.Query);
 

            var client = Util.GetBfsapiSoapClient(config);
            var accounts = await client.GetAccountsAsync(getAccountsRequest);

            return Json( accounts.Result );

        }
 
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using System.Reflection;
using BFSAPO.NordFK;
namespace Glofitech.BFSAbsorber.Nord.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class PositionsController : Controller
    {
        IConfiguration config;
        private IHttpContextAccessor _accessor;

        public PositionsController(IConfiguration iconfiguration, IHttpContextAccessor accessor)
        {
            config = iconfiguration;
            _accessor = accessor;
        }



        // GET api/values
        [HttpGet]
        public BFSAPO.NordFK.GetPositionResponse Get()
        {
            string ipAddress = HttpContext.Connection.RemoteIpAddress.ToString().Trim();

            if (!Util.IsIpAddressAllowed(ipAddress, config))
            {
                List<string> a = new List<string>();
                a.Add(ipAddress);
                return null;
            }


            string uName = "";
            string uPass = "";
            string bfsApiKey = "";

            uName = config["APIClientDetails:UserName"];
            uPass = config["APIClientDetails:Password"];
            bfsApiKey = config["APIClientDetails:APIKey"];

            var client = Util.GetBfsapiSoapClient(config);


            var credentials = new BFSAPO.NordFK.Credentials()
            {
                UserName = uName,
                Password = uPass,

            };


            var allFields = new BFSAPO.NordFK.GetPositionFields();

            foreach (var p in allFields.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {

                p.SetValue(allFields, true);
            }

            GetPositionRequest request = new GetPositionRequest();
            request.Fields = allFields;
            request.Credentials = new Credentials();
            request.Credentials.UserName = config["APIClientDetails:UserName"];
            request.Credentials.Password = config["APIClientDetails:Password"];
            request.identify = config["APIClientDetails:APIKey"];

            request.Args = new GetPositionArgs();
 

            request.Args = (GetPositionArgs)Util.GetArgsFromQueryCollection(request.Args, HttpContext.Request.Query);



            if (request.Args.BalanceDate == null)
                request.Args.BalanceDate = DateTime.Today.Date;

            if (request.Args.DisplayCurrencyCode == null)
                request.Args.DisplayCurrencyCode = "SEK";

            if (request.Args.AccountDimensionKey == null)
                request.Args.AccountDimensionKey = "T";


            var historicPositions = client.GetPositionsAsync(request);

            return historicPositions.Result;
        }

     

    }
}

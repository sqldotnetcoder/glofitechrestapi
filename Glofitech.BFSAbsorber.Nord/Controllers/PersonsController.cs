﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Authorization;
using BFSAPO.NordFK;
using System.Reflection;

namespace Glofitech.BFSAbsorber.Nord.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    public class PersonsController : Controller
    {
        IConfiguration config;
        private IHttpContextAccessor _accessor;

        public PersonsController(IConfiguration iconfiguration, IHttpContextAccessor accessor)
        {
            config = iconfiguration;
            _accessor = accessor;
        }



        // GET api/values
        [HttpGet]
        public async Task< BFSAPO.NordFK.GetPersonResponseRow[]> Get()
        {
            string ipAddress = HttpContext.Connection.RemoteIpAddress.ToString().Trim();

            if (!Util.IsIpAddressAllowed(ipAddress, config))
            {
                List<string> a = new List<string>();
                a.Add(ipAddress);
                return null;
            }


            string uName = "";
            string uPass = "";
            string bfsApiKey = "";

            uName = config["APIClientDetails:UserName"];
            uPass = config["APIClientDetails:Password"];
            bfsApiKey = config["APIClientDetails:APIKey"];

            var client = Util.GetBfsapiSoapClient(config);

            var credentials = new BFSAPO.NordFK.Credentials()
            {
                UserName = uName,
                Password = uPass,
            };



            var allFields = new BFSAPO.NordFK.GetPersonFields();
            foreach (var p in allFields.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {

                p.SetValue(allFields, true);
            }

            GetPersonRequest request = new GetPersonRequest();
            request.Fields = allFields;
            request.Credentials = new Credentials();
            request.Credentials.UserName = config["APIClientDetails:UserName"];
            request.Credentials.Password = config["APIClientDetails:Password"];
            request.identify = config["APIClientDetails:APIKey"];

            request.Args = new GetPersonArgs();



            object value = new object();

            request.Args = (GetPersonArgs)Util.GetArgsFromQueryCollection(request.Args, HttpContext.Request.Query);




            var persons =await client.GetPersonsAsync( request            );

            return persons.Result ;
        }

    

       
      
 
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Glofitech.BFSAbsorber.Nord.Models
{
    public class APIClientDetails
    {
        public String UserName { get; set; }
        public String Password { get; set; }
        public String APIKey { get; set; }

        public APIClientDetails() { }
    }
}
